defmodule Hangman do
  def score_guess({_,_,_,0} = game, _guess), do: game
  def score_guess({palabra, correctos, incorrectos, turnos} = game, guess) do
    cond do
      correctos =~ guess or incorrectos =~ guess -> game
      palabra =~ guess -> {palabra, correctos <> guess, incorrectos, turnos}
      true -> {palabra, correctos, incorrectos <> guess, turnos - 1}
    end
  end

  def format_feedback({palabra, correctas, _, _}) do
    palabra
    |> String.graphemes()
    |> Enum.map(fn l -> if correctas =~ l, do: l, else: "-" end)
    |> Enum.join()
  end
end
