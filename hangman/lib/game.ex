defmodule Game do
  use GenServer

  def start_link(name \\ :name) do
    word = Dictionary.random_word() |> String.trim()
    GenServer.start_link(__MODULE__, word, name: name)
  end

  def init(word), do: {:ok, {word, "", "", 9}}

  def submit_guess(pid, guess), do: GenServer.cast(pid, {:submit_guess, guess})
  def get_feedback(pid), do: GenServer.call(pid, :get_feedback)

  def handle_cast({:submit_guess, letter}, state) do
    {:noreply, Hangman.score_guess(state, letter)}
  end

  def handle_call(:get_feedback, _from, {palabra, _, _, turnos} = state) do
    feedback = Hangman.format_feedback(state)
    {
      :reply,
      %{
        feedback: feedback,
        remaining_turns: turnos,
        status:
          cond do
            feedback == palabra and turnos > 0 -> :win
            turnos == 0 -> :lose
            true -> :playing
          end
      },
      state}
  end
end
